package app

import cask._
import models._

object RestfulAPIServer extends MainRoutes  {
  override def host: String = "0.0.0.0"
  override def port: Int = 4000

  @get("/")
  def root(): Response = {
    JSONResponse("Ok")
  }

  @get("/api/locations")
  def locations(): Response = {
    JSONResponse(Location.all.map(location => location.toMap))
  }

  @postJson("/api/locations")
  def locations(name: String, coordX: Int, coordY: Int): Response = {
    if (Location.exists("name", name)) {
      return JSONResponse("Existing location", 409)
    }

    val location = Location(name, coordX, coordY)
    location.save()
    JSONResponse(location.id)
  }

  @get("/api/consumers")
  def consumers(): Response = {
    JSONResponse(Consumer.all.map(consumer => consumer.toMap))
  }

  @postJson("/api/consumers")
  def consumers(username: String, locationName: String): Response = {
    if (Consumer.exists("username", username) ||
        Provider.exists("username", username)) {
      return JSONResponse("Existing Username", 409)
    }
    if (!Location.exists("name", locationName)) {
      return JSONResponse("Non Existing Location", 404)
    }
    val consumer = Consumer(username, locationName)
    consumer.save()
    JSONResponse(consumer.id)
  }

  @get("/api/providers")
  def providers(locationName: String = ""): Response = {
    if (locationName.isEmpty) {
    return JSONResponse(Provider.all.map(provider => provider.toMap))
    }
    if (Provider.exists("locationName", locationName)) {
      val n = Map("locationName" -> locationName)
      return JSONResponse(Provider.filter(n).map(provider => provider.toMap))
    } else{
      return JSONResponse("Non existing location", 404)
    }
  }

  @postJson("/api/providers")
  def providers(username: String, locationName: String, storeName: String,
    maxDeliveryDistance: Int): Response = {
    if (Provider.exists("username", username) ||
      Provider.exists("storeName", storeName) ||
      Consumer.exists("username", username)) {
      return JSONResponse("Existing Username/StoreName", 409)
    }
    if (maxDeliveryDistance < 0) {
      return JSONResponse("Negative maxDeliveryDistance", 400)
    }
    if (!Location.exists("name", locationName)) {
      return JSONResponse("Non Existing Location", 404)
    }

    val provider = Provider(username, locationName, storeName, maxDeliveryDistance)
    provider.save()
    JSONResponse(provider.id)
  }

  @post("/api/users/delete/:username")
  def delete(username: String): Response = {
    val n = Map("username" -> username)
    val user = Consumer.filter(n) ++ Provider.filter(n)
    if (user.isEmpty) {
      return JSONResponse("Non existent user", 404)
    } else {
      val userid = user.head.id
      if (user.head.isProvider) {
        Provider.delete(userid)
      } else {
        Consumer.delete(userid)
      }
    }
    JSONResponse("Ok", 200)
  }

  @get("/api/items")
  def items(providerUsername: String = ""): Response = {
    if (providerUsername.isEmpty) {
      return JSONResponse(Item.all.map(item => item.toMap))
    }
    if (Provider.exists("username", providerUsername)) {
      val n = Map("providerUsername" -> providerUsername)
      return JSONResponse(Item.filter(n).map(location => location.toMap))
    } else{
      return JSONResponse("Non Existing Provider", 404)
    }
  }

  @postJson("/api/items")
  def items(name: String, description: String, price: Float, providerUsername: String): Response = {
    if (!Provider.exists("username", providerUsername)) {
      return JSONResponse("Non Existing Provider", 404)
    }
    if (price < 0) {
      return JSONResponse("Negative Price", 400)
    }
    val n = Map("providerUsername" -> providerUsername, "name" -> name)
    val itemsByProvider = Item.filter(n)
    if (!itemsByProvider.isEmpty) {
      return JSONResponse("Existing Item for Provider", 409)
    }
    val item = Item(name, description, price, providerUsername)
    item.save()
    JSONResponse(item.id)
  }

  @post("/api/items/delete/:itemId")
  def deleteitems(itemId: Int): Response = {
    val n = Map("id" -> itemId)
    val item = Item.filter(n)
    if (item.isEmpty) {
      return JSONResponse("Non Existent Item", 404)
    } else {
      Item.delete(itemId)
    }
    JSONResponse("Ok", 200)
  }

  @get("/api/orders")
  def orders(username: String = ""): Response = {
    if (username.isEmpty) {
      return JSONResponse(Order.all.map(item => item.toMap))
    }
    val n = Map("username" -> username)
    val user = Consumer.filter(n) ++ Provider.filter(n)

    if (user.isEmpty) {
      return JSONResponse("Non Existing User", 404)
    } else {
      if (user.head.isProvider) {
        val m = Map("providerUsername" -> user.head.username)
        return JSONResponse(Order.filter(m).map(location => location.toMap))
      } else {
        val m = Map("consumerUsername" -> user.head.username)
        return JSONResponse(Order.filter(m).map(location => location.toMap))
      }
    }
  }

  @get("/api/orders/detail/:orderId")
  def detailorders(orderId: Int): Response = {
    if (Order.exists("id", orderId)) {
      val n = Map("id" -> orderId)
      val order = Order.filter(n).head
      val details = order.getListDetails
      return JSONResponse(details)
    } else{
      return JSONResponse("Non Existing Order", 404)
    }
  }

  @postJson("/api/orders")
  def orders(providerUsername: String, consumerUsername: String,
    items: List[ItemList]): Response = {
    if (!Provider.exists("username", providerUsername)) {
      return JSONResponse("Non Existing Provider", 404)
    }
    if (!Consumer.exists("username", consumerUsername)) {
      return JSONResponse("Non Existing Consumer", 404)
    }
    val n = Map("username" -> providerUsername)
    val provider = Provider.filter(n).head
    val m = Map("username" -> consumerUsername)
    val consumer = Consumer.filter(m).head

    val order = Order(providerUsername, consumerUsername, items, consumer.id,
      consumer.locationName, provider.id, provider.storeName)

    val total = order.getTotal match {
      case Some(1) => return JSONResponse("Negative Amount", 400)
      case Some(2) => return JSONResponse("Non Existing Item for Provider", 404)
      case _ => order.orderTotal
    }
    provider.addMoney(total)
    consumer.takeMoney(total)
    order.save()
    JSONResponse(order.id)
  }

  @post("/api/orders/delete/:orderId")
  def deleteorders(orderId: Int): Response = {
    if (Order.exists("id", orderId)) {
      Order.delete(orderId)
    } else {
      return JSONResponse("Non Existent Order", 404)
    }
    JSONResponse("Ok", 200)
  }

  @post("/api/orders/deliver/:id")
  def deliver(id: Int): Response = {
    val order = Order.find(id) match {
      case Some(e) => e
      case None => return JSONResponse("Non Existing Order", 404)
    }
    order.deliver
    JSONResponse("Ok", 200)
  }

  override def main(args: Array[String]): Unit = {
    System.err.println("\n " + "=" * 39)
    System.err.println(s"| Server running at http://$host:$port ")

    if (args.length > 0) {
      val databaseDir = args(0)
      Database.loadDatabase(databaseDir)
      System.err.println(s"| Using database directory $databaseDir ")
    } else {
      Database.loadDatabase()  // Use default location
    }
    System.err.println(" " + "=" * 39 + "\n")

    super.main(args)
  }

  initialize()
}
