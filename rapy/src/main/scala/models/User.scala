package models

trait User {
  protected var balance: Int = 0
  val username: String
  val locationName: String
  def isProvider: Boolean = false
}

object Consumer extends ModelCompanion[Consumer] {
  protected def dbTable: DatabaseTable[Consumer] = Database.consumers

   def apply(username: String, locationName: String): Consumer =
     new Consumer(username, locationName)

   private[models] def apply(jsonValue: JValue): Consumer = {
     val value = jsonValue.extract[Consumer]
     value._id = (jsonValue \ "id").extract[Int]
     value.balance = (jsonValue \ "balance").extract[Int]
     value.save()
     value
   }
 }

class Consumer(val username: String, val locationName: String) extends Model[Consumer] with User{
  protected def dbTable: DatabaseTable[Consumer] = Consumer.dbTable
  override def toMap: Map[String, Any] = super.toMap +
    ("username" -> username, "locationName" -> locationName, "balance" -> balance)
  override def toString: String = s"User: $username"

  def takeMoney(total: Float): Unit = {
    balance -= total.toInt
    save()
  }
}

object Provider extends ModelCompanion[Provider] {
  protected def dbTable: DatabaseTable[Provider] = Database.providers

   def apply(username: String, locationName: String, storeName: String, maxDeliveryDistance: Int):
    Provider =
      new Provider(username, locationName, storeName, maxDeliveryDistance)

   private[models] def apply(jsonValue: JValue): Provider = {
     val value = jsonValue.extract[Provider]
     value._id = (jsonValue \ "id").extract[Int]
     value.balance = (jsonValue \ "balance").extract[Int]
     value.save()
     value
   }
 }

class Provider(val username: String, val locationName: String, val storeName: String,
  val maxDeliveryDistance: Int) extends Model[Provider] with User {
  protected def dbTable: DatabaseTable[Provider] = Provider.dbTable
  override def toMap: Map[String, Any] = super.toMap + ("username" -> username,
    "locationName" -> locationName, "storeName" -> storeName,
     "maxDeliveryDistance" -> maxDeliveryDistance, "balance" -> balance)
  override def toString: String = s"User: $username, StoreName: $storeName"
  override def isProvider: Boolean = true

  def addMoney(total: Float): Unit = {
    balance += total.toInt
    save()
  }
}
