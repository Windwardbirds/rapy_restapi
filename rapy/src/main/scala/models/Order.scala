package models
import upickle.default.{ReadWriter => RW, macroRW}

object Order extends ModelCompanion[Order] {
  protected def dbTable: DatabaseTable[Order] = Database.orders

   def apply(providerUsername: String, consumerUsername: String, items: List[ItemList],
   consumerId: Int, consumerLocation: String, providerId: Int,
   providerStoreName: String): Order =
     new Order(providerUsername, consumerUsername, items, consumerId,
       consumerLocation, providerId, providerStoreName)

   private[models] def apply(jsonValue: JValue): Order = {
     val value = jsonValue.extract[Order]
     value._id = (jsonValue \ "id").extract[Int]
     value.status = (jsonValue \ "status").extract[Option[String]]
     value.orderTotal = (jsonValue \ "orderTotal").extract[Float]
     value.save()
     value
   }
 }


class Order(val providerUsername: String, val consumerUsername: String,
  val items: List[ItemList], val consumerId: Int, consumerLocation: String,
  val providerId: Int, val providerStoreName: String) extends Model[Order]{
  var status: Option[String] = Some("payed")
  var orderTotal: Float = 0
  protected def dbTable: DatabaseTable[Order] = Order.dbTable

  override def toMap: Map[String, Any] = super.toMap + ("providerUsername" -> providerUsername,
    "consumerUsername" -> consumerUsername, "consumerId" -> consumerId, "items" -> items,
    "consumerLocation" -> consumerLocation, "providerId" -> providerId,
    "providerStoreName" -> providerStoreName, "orderTotal" -> orderTotal, "status" -> status)

  override def toString: String = s"Order: $providerUsername, $consumerUsername, $items"

  def getTotal: Option[Int] = {
    var sum : Float = 0
    items.foreach{ x =>
      if (x.amount < 0) {
        return Some(1)
      }
      val o = Map("providerUsername" -> providerUsername, "name" -> x.name)
      val item = Item.filter(o)
      if (item.isEmpty) {
        return Some(2)
      }
      val total: Float = item.head.price * x.amount
      sum += total
    }
    orderTotal = sum
    return None
  }

  def getListDetails : List[Map[String, Any]] = {
    items.map(it => getItemDetails(it))
  }

  def getItemDetails(il: ItemList) : Map[String, Any] = {
    val n = Map("providerUsername" -> providerUsername, "name" -> il.name)
    val item = Item.filter(n).head
    val resultmap = Map("itemid" -> item.id, "name" -> item.name,
       "description" -> item.description, "price" -> item.price, "amount" -> il.amount)
    return resultmap
  }

  def deliver: Unit = {
  status = Some("delivered")
  save()
  }
}

case class ItemList(val name: String, val amount: Int)
  object ItemList{
    implicit val rw: RW[ItemList] = macroRW
  }
