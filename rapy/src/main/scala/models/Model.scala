package models

trait ModelCompanion[M <: Model[M]] {
  protected def dbTable: DatabaseTable[M]

  private[models] def apply(jsonValue: JValue): M

  def all: List[M] = dbTable.instances.values.toList

  def find(id: Int): Option[M] = all.find{_.id == id}

  def exists(attr: String, value: Any): Boolean = {
    def sats(m : M) : Boolean = {m.toMap.exists(_ == attr -> value)}
    // exists (p : (m : M) => Boolean)
    all.exists(sats)
  }

  def delete(id: Int): Unit = {
    dbTable.delete(id)
  }

  def filter(mapOfAttributes: Map[String, Any]): List[M] = {
    def asts(m : M) : Boolean = {
      mapOfAttributes.forall{ x =>
        val (key, value) = x
        m.toMap.exists(_ == key -> value)
      }
    }
    all.filter(asts)
  }
}

trait Model[M <: Model[M]] { self: M =>
  protected var _id: Int = 0

  def id: Int = _id

  protected def dbTable: DatabaseTable[M]

  def toMap: Map[String, Any] = Map("id" -> _id)

  def save(): Unit = {
    if (_id == 0) { _id = dbTable.getNextId }
    dbTable.save(this)
  }
}
