
# LABORATORIO PROGRAMACIÓN ORIENTADA A OBJETOS CON SCALA

En este laboratorio se utilizó RESTED para probar los GET y POST de la aplicación.

## Compilación

$sbt run

## Librerías

Se importó la librería upickle.default.{ReadWriter => RW, macroRW} para serializar diccionarios JSON, donde las claves son los nombres de cada campo. Se definió *case class ItemList* en Order.scala para poder utilizarlo en el POST /api/orders.

## Conceptos usados en el proyecto

* Encapsulamiento

El encapsulamiento nos permitió trabajar por separado las implementaciones. En RestfulAPI sólo creamos definiciones utilizando los métodos que declaramos en Model y los demás archivos. De no haber tenido encapsulamiento, el usuario (GET y POST) hubiera tenido que manejar las bases de datos directamente haciendo que el código sea repetitivo y redundante.

*Se denomina encapsulamiento al ocultamiento del estado, es decir, de los datos miembro de un objeto de manera que solo se pueda cambiar mediante las operaciones definidas para ese objeto.*
*De esta forma el usuario de la clase puede obviar la implementación de los métodos y propiedades para concentrarse solo en cómo usarlos.*

* Herencia (funciona igual que en Java) y Traits

Usar herencia no sólo disminuyó la cantidad de código repetitivo sino que evitó que definiéramos en cada clase el mismo método. Por ejemplo el método exists() está dentro de un trait y es heredado por todas las clases.

*Es la capacidad de crear clases que adquieren de manera automática los miembros (atributos y métodos) de otras clases que ya existen, pudiendo al mismo tiempo añadir atributos y métodos propios.*

*Un trait encapsula definiciones de métodos y campos, que se pueden reutilizar mezclándolos dentro de clases.*

* Sobrecarga de operadores

Scala no tiene sobrecarga de operadores, pero podríamos considerar el uso de *override* para sobreescribir las funciones. Por ejemplo, toMap(), está redefinido en cada clase, por lo que dependiendo el contexto, es el resultado que obtenemos(que es la definición de sobrecarga).

*La sobrecarga se refiere a la posibilidad de tener dos o más funciones con el mismo nombre pero funcionalidad diferente. Es decir, dos o más funciones con el mismo nombre realizan acciones diferentes. El compilador usará una u otra dependiendo de los parámetros usados.*
*Scala técnicamente no tiene sobrecarga de operadores, porque como es un lenguaje orientado a objetos y no tiene operadores en el sentido tradicional.*

* Polimorfismo

Scala permite el uso de polimorfismo paramétrico, el cual usamos cuando pasamos un Tipo como parámetro a una función. Esto nos facilita no tener que definir los parámetros en cada función sino que usamos por ejemplo: [M], que sólo indica que es una base de datos y no cuál es.

*El polimorfismo se refiere a la propiedad por la que es posible enviar mensajes sintácticamente iguales a objetos de tipos distintos. El único requisito que deben cumplir los objetos que se utilizan de manera polimórfica es saber responder al mensaje que se les envía.*
*Polimorfismo paramétrico: el código toma un tipo como parámetro, bien explícita o implicitamente. A esta característica también se conoce como genéricos.*
